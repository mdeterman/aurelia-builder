FROM node:9-alpine

ENV VERSION=0.33.1

MAINTAINER Mark Determan <mark@determan.io>

RUN apk update && apk upgrade && \
apk add --no-cache bash git openssh

RUN npm install aurelia-cli@${VERSION} -g

CMD ["au", "--version"]